<?php
require './theme/layot/head.php';
?>
    <!-- Page content-->
    <div class="container">
        <div class="text-center mt-5">
            <h1><?= $params['tittle'] ?></h1>
        </div>
        <div class="inner order-1 py-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="h2 pb-2 font-weight--bold">Офисы Ingate</div>
                        <p class="font-color--gray my-4">Мы работаем по будням, с 9:00 до 19:30</p>
                        <div class="map-panel my-5">
                            <div class="i-tabs" itemscope="" itemtype="http://schema.org/Organisation">
                                <span class="d-none" itemprop="name">Ingate</span>
                                <div class="i-tabs-nav flex">
                                    <div class="i-tab-nav">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="20"
                                             fill="currentColor"
                                             class="bi bi-house-heart" viewBox="0 0 16 16">
                                            <use xlink:href="#address"></use>
                                            <path fill-rule="evenodd"
                                                  d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.707L2 8.207V13.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V8.207l.646.646a.5.5 0 0 0 .708-.707L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.707 1.5ZM13 7.207l-5-5-5 5V13.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7.207Zm-5-.225C9.664 5.309 13.825 8.236 8 12 2.175 8.236 6.336 5.309 8 6.982Z"/>
                                        </svg>
                                        <a data-map="moscow" itemprop="addressLocality" class="city collapsed"
                                           href="#map-city-moscow" data-toggle="collapse" data-target="#map-city-moscow"
                                           aria-expanded="false" aria-controls="moscow">Москва</a>,
                                        <span itemprop="streetAddress">Каширское ш., 3, корп. 2, стр. 4, оф. 53</span>,
                                        <a href="tel:+78124160927"><span itemprop="telephone">+7 (812) 416 09 27</span></a>

                                        <!--                                <a href="tel:+74951510683" class="comagic_phone">+7 (495) 151-06-83</a>-->
                                    </div>
                                    <div class="i-tab-nav">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="20"
                                             fill="currentColor"
                                             class="bi bi-house-heart" viewBox="0 0 16 16">
                                            <use xlink:href="#address"></use>
                                            <path fill-rule="evenodd"
                                                  d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.707L2 8.207V13.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V8.207l.646.646a.5.5 0 0 0 .708-.707L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.707 1.5ZM13 7.207l-5-5-5 5V13.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7.207Zm-5-.225C9.664 5.309 13.825 8.236 8 12 2.175 8.236 6.336 5.309 8 6.982Z"/>
                                        </svg>
                                        <a data-map="spb" itemprop="addrecLocality" class="city" href="#map-city-spb"
                                           data-toggle="collapse" data-target="#map-city-spb" aria-expanded="false"
                                           aria-controls="spb">Санкт-Петербург</a>,
                                        <span itemprop="streetAddress">ул. Марата, д. 49</span>,
                                        <a href="tel:+78124160927"><span itemprop="telephone">+7 (812) 416 09 27</span></a>
                                    </div>
                                    <div class="i-tab-nav active">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="20"
                                             fill="currentColor"
                                             class="bi bi-house-heart" viewBox="0 0 16 16">
                                            <use xlink:href="#address"></use>
                                            <path fill-rule="evenodd"
                                                  d="M8.707 1.5a1 1 0 0 0-1.414 0L.646 8.146a.5.5 0 0 0 .708.707L2 8.207V13.5A1.5 1.5 0 0 0 3.5 15h9a1.5 1.5 0 0 0 1.5-1.5V8.207l.646.646a.5.5 0 0 0 .708-.707L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.707 1.5ZM13 7.207l-5-5-5 5V13.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7.207Zm-5-.225C9.664 5.309 13.825 8.236 8 12 2.175 8.236 6.336 5.309 8 6.982Z"/>
                                        </svg>
                                        <a data-map="tula" itemprop="addressLocality" class="city" href="#map-city-tula"
                                           data-toggle="collapse" data-target="#map-city-tula" aria-expanded="true"
                                           aria-controls="tula">Тула</a>,
                                        <span itemprop="streetAddress">ул. Пушкинская, 27</span>,
                                        <a href="tel:+74872250221"><span itemprop="telephone">+7 (4872) 25 02 21</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <? if (!$params['result']) { ?>
            <h2>Свяжитесь с нами</h2>
            <div class="container my-5">
                <div class="row">
                    <div class="col-md-6">
                        <form>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1"
                                       aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1">
                            </div>
                            <div class="mb-3 form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Remember me</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

            <?
        } else { ?>
            <p>С вами свяжуться в ближайщее время</p>
        <? } ?>
    </div>
<? require './theme/layot/footer.php';
