<?php
require './theme/layot/head.php';
?>
    <!-- Page content-->
    <di
    <div class="container">
        <div class="text-center mt-5">
            <h1>Главная</h1>
        </div>
        <section style="padding-bottom: 60px;" class="our-webcoderskull padding-lg">
            <div class="container">
                <div class="row mt-5 text-start d-flex justify-content-start">
                    <h2>Наша команда</h2>
                </div>
                <div class="row d-flex">
                    <div class="col-12 col-md-3">
                        <div class="cnt-block equal-hight" style="height:auto;">
                            <figure>
                                <img src="http://www.webcoderskull.com/img/team4.png" class="img-fluid" alt="">
                            </figure>
                            <h3>
                                <a href="http://www.webcoderskull.com/">Web coder skull</a>
                            </h3>
                            <p> Frelance Web Developer</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="cnt-block equal-hight" style="height:auto;">
                            <figure>
                                <img src="http://www.webcoderskull.com/img/team1.png" class="img-fluid" alt="">
                            </figure>
                            <h3>
                                <a href="http://www.webcoderskull.com/">Kappua</a>
                            </h3>
                            <p> Frelance Web Developer</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="cnt-block equal-hight" style="height:auto;">
                            <figure>
                                <img src="http://www.webcoderskull.com/img/team4.png" class="img-fluid" alt="">
                            </figure>
                            <h3>
                                <a href="http://www.webcoderskull.com/">Manish</a>
                            </h3>
                            <p> Frelance Web Developer</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="cnt-block equal-hight" style="height:auto;">
                            <figure>
                                <img src="http://www.webcoderskull.com/img/team2.png" class="img-fluid" alt="">
                            </figure>
                            <h3>
                                <a href="http://www.webcoderskull.com/">Atul</a>
                            </h3>
                            <p> Frelance Web Developer</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <style>
        .glyphicon {
            margin-right: 5px;
        }

        .thumbnail {
            margin-bottom: 20px;
            padding: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px
        }

        .item.list-group-item {
            float: none;
            width: 100%;
            background-color: #fff;
            margin-bottom: 10px
        }

        .item.list-group-item:nth-of-type(odd):hover, .item.list-group-item:hover {
            background: rgb(105, 65, 152)
        }

        .item.list-group-item .list-group-image {
            margin-right: 10px
        }

        .item.list-group-item .thumbnail {
            margin-bottom: 0px
        }

        .item.list-group-item .caption {
            padding: 9px 9px 0px 9px
        }

        .item.list-group-item:nth-of-type(odd) {
            background: #eeeeee
        }

        .item.list-group-item:before, .item.list-group-item:after {
            display: table;
            content: " "
        }

        .item.list-group-item img {
            float: left
        }

        .item.list-group-item:after {
            clear: both
        }

        .list-group-item-text {
            margin: 0 0 11px
        }
    </style>

    <div class="container">
        <div class="well well-sm mb-2">
            <strong>Вид:</strong>
            <div class="btn-group "></div>
            <a href="#" id="list" class="btn btn-default ">
                Список
            </a>
            <a href="#" id="grid" class="btn btn-default ">
                Сетка
            </a>
        </div>
        <div class="row mb-5">
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="thumbnail">
                    <div class="product-thumb">
                        <img src="https://bootstraptema.ru/images/type/400x250.png" alt="">
                    </div>
                    <div class="product-details mt-2">
                        <h4>Товар 1</h4>
                        <p>Описание товара 1, Описание товара 1, Описание товара 1, Описание товара 1, Описание товара
                            1,
                            Описание товара 1, Описание товара 1, Описание товара 1, Описание товара 1. </p>
                        <div class="product-bottom-details d-flex justify-content-between">
                            <div class="product-price">
                                <h5>$350.00</h5>
                            </div>
                            <div class="product-links pe-4">
                                <a class="btn btn-success" href="#">Заказать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="thumbnail">
                    <div class="product-thumb">
                        <img src="https://bootstraptema.ru/images/type/400x250.png" alt="">
                    </div>
                    <div class="product-details mt-2">
                        <h4>Товар 2</h4>
                        <p>Описание товара 2, Описание товара 2, Описание товара 2, Описание товара 2, Описание товара
                            2,
                            Описание товара 2, Описание товара 2, Описание товара 2, Описание товара 2.</p>
                        <div class="product-bottom-details d-flex justify-content-between">
                            <div class="product-price">
                                <h5>$250.00</h5>
                            </div>
                            <div class="product-links pe-4">
                                <a class="btn btn-success" href="#">Заказать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="thumbnail">
                    <div class="product-thumb">
                        <img src="https://bootstraptema.ru/images/type/400x250.png" alt="">
                    </div>
                    <div class="product-details mt-2">
                        <h4>Товар 3</h4>
                        <p>Описание товара 3, Описание товара 3, Описание товара 3, Описание товара 3, Описание товара
                            3,
                            Описание товара 3, Описание товара 3, Описание товара 3, Описание товара 3.</p>
                        <div class="product-bottom-details d-flex justify-content-between">
                            <div class="product-price">
                                <h5>$150.00</h5>
                            </div>
                            <div class="product-links pe-4">
                                <a class="btn btn-success" href="#">Заказать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="thumbnail">
                    <div class="product-thumb">
                        <img src="https://bootstraptema.ru/images/type/400x250.png" alt="">
                    </div>
                    <div class="product-details mt-2">
                        <h4>Товар 4</h4>
                        <p>Описание товара 4, Описание товара 4, Описание товара 4, Описание товара 4, Описание товара
                            4,
                            Описание товара 4, Описание товара 4, Описание товара 4, Описание товара 4.</p>
                        <div class="product-bottom-details d-flex justify-content-between">
                            <div class="product-price">
                                <h5>$200.00</h5>
                            </div>
                            <div class="product-links pe-4">
                                <a class="btn btn-success" href="#">Заказать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="thumbnail">
                    <div class="product-thumb">
                        <img src="https://bootstraptema.ru/images/type/400x250.png" alt="">
                    </div>
                    <div class="product-details mt-2">
                        <h4>Товар 5</h4>
                        <p>Описание товара 5, Описание товара 5, Описание товара 5, Описание товара 5, Описание товара
                            5,
                            Описание товара 5, Описание товара 5, Описание товара 5, Описание товара 5.</p>
                        <div class="product-bottom-details d-flex justify-content-between">
                            <div class="product-price">
                                <h5>$320.00</h5>
                            </div>
                            <div class="product-links pe-4">
                                <a class="btn btn-success " href="#">Заказать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="thumbnail">
                    <div class="product-thumb">
                        <img src="https://bootstraptema.ru/images/type/400x250.png" alt="">
                    </div>
                    <div class="product-details mt-2">
                        <h4>Товар 6</h4>
                        <p>Описание товара 6, Описание товара 6, Описание товара 6, Описание товара 6, Описание товара
                            6,
                            Описание товара 6, Описание товара 6, Описание товара 6, Описание товара 6.</p>
                        <div class="product-bottom-details d-flex justify-content-between">
                            <div class="product-price">
                                <h5>$21.00</h5>
                            </div>
                            <div class="product-links pe-4">
                                <a class="btn btn-success" href="#">Заказать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#list').click(function (event) {
                event.preventDefault();
                $('#products .item').addClass('list-group-item');
            });
            $('#grid').click(function (event) {
                event.preventDefault();
                $('#products .item').removeClass('list-group-item');
                $('#products .item').addClass('grid-group-item');
            });
        });
    </script>

    </div>
    <!--
<pre>
    <?php
    print_r($_SERVER);
    ?>
</pre>
-->
<? require './theme/layot/footer.php';
